package com.tradisys.games.server;

public class HttpClientConfig {
    private int poolMaxTotal = 0;
    private int poolDefaultMaxPerRoute = 0;
    private int poolMaxPerBlkChtRoute = 0;
    private int connectTimeout = 0;
    private int socketTimeout = 0;
    private int connectionRequestTimeout = 0;

    public static HttpClientConfig getDefault() {
        HttpClientConfig cfg = new HttpClientConfig();
        cfg.socketTimeout = 20 * 1000;
        cfg.connectTimeout = 20 * 1000;
        cfg.connectionRequestTimeout = 20 * 1000;
        cfg.poolMaxTotal = 100;
        cfg.poolDefaultMaxPerRoute = 50;
        cfg.poolMaxPerBlkChtRoute = 50;
        return cfg;
    }

    public int getPoolMaxTotal() {
        return poolMaxTotal;
    }

    public void setPoolMaxTotal(int poolMaxTotal) {
        this.poolMaxTotal = poolMaxTotal;
    }

    public int getPoolDefaultMaxPerRoute() {
        return poolDefaultMaxPerRoute;
    }

    public void setPoolDefaultMaxPerRoute(int poolDefaultMaxPerRoute) {
        this.poolDefaultMaxPerRoute = poolDefaultMaxPerRoute;
    }

    public int getPoolMaxPerBlkChtRoute() {
        return poolMaxPerBlkChtRoute;
    }

    public void setPoolMaxPerBlkChtRoute(int poolMaxPerBlkChtRoute) {
        this.poolMaxPerBlkChtRoute = poolMaxPerBlkChtRoute;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public int getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }
}
