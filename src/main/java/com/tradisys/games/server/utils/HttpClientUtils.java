package com.tradisys.games.server.utils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

import java.util.Arrays;

public class HttpClientUtils {
    public static void logBadResponse(CloseableHttpResponse response, Logger logger) {
        try {
            StringBuilder headers = new StringBuilder(100);
            String content = "no-content";

            Arrays.stream(response.getAllHeaders()).forEach(h -> {
                headers.append(h.getName()).append(": ").append(h.getValue()).append('\n');
            });
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
                content = EntityUtils.toString(httpEntity);
            }

            int statusCode = response.getStatusLine() != null ? response.getStatusLine().getStatusCode() : -1;
            logger.trace("Http response status code is not OK: http_status={} headers={} content={}",
                    statusCode, headers.toString(), content);
        } catch (Throwable ex) {
            logger.error("Http response status code is not OK: error during reading body", ex);
        }
    }
}
