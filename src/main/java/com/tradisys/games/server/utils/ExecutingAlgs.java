package com.tradisys.games.server.utils;

import com.tradisys.games.server.exception.BlkChInterruptedException;
import com.tradisys.games.server.exception.BlkChTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

import static com.tradisys.games.server.utils.FormatUtils.formatInterval;

public class ExecutingAlgs {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutingAlgs.class);

    private static ThreadPoolExecutor executor = new ThreadPoolExecutor(10, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS, new SynchronousQueue<>());

    public static ThreadPoolExecutor pool() {
        return executor;
    }

    public static <T> T firstSuccessTask(List<? extends Executable<T>> executables, long timeout) throws IOException {

        if (timeout <= 0) {
            throw new IllegalArgumentException("Timeout must be greater than 0");
        }

        Set<Future<T>> futures = new HashSet<>(executables.size() + 1);
        CompletionService<T> completionService = new ExecutorCompletionService<>(executor);

        Map<Future<T>, Wrapper<T>> map = new HashMap<>();

        for (Executable<T> exec: executables) {
            final Wrapper<T> w = new Wrapper<>(exec);
            Future<T> f = completionService.submit(w::call);
            futures.add(f);
            map.put(f, w);
        }

        try {
            long pause = 500;
            int ticks = 0;

            while (futures.size() > 0) {
                Future<T> f = completionService.poll();

                if (f != null) {
                    Wrapper<T> w = map.get(f);
                    try {
                        T resp = f.get(5, TimeUnit.SECONDS);
                        if (w.exec.isSuccess(resp)) {
                            return resp; // remaining tasks will be cancelled in finally
                        }
                    } catch (ExecutionException ex) {
                        LOGGER.trace("Executable failed: {}", w.exec.info(), ex);
                        w.setError(ex.getCause());
                    } catch (TimeoutException ex) {
                        LOGGER.error("Impossible situation was tracked just in case: ", w.exec.info(), ex);
                        w.setError(ex);
                    } finally {
                        // response received
                        futures.remove(f);
                    }
                } else if (ticks * pause > timeout) {
                    // valid response was not received in specified timeout - need to stop
                    String status = status(futures, map, timeout).toString();
                    LOGGER.debug(status);
                    throw new BlkChTimeoutException(status);
                }

                Thread.sleep(pause);
                ticks++;
            }
        } catch (InterruptedException ex) {
            // stop execution - clean up is done in finally block
            String status = status(futures, map, timeout).toString();
            LOGGER.warn("Request to terminate thread was sent - all communications were stopped: {}", status, ex);
            throw new BlkChInterruptedException("Request to terminate thread was sent - all communications were stopped", ex);
        } finally {
            futures.forEach(t -> t.cancel(true));
        }

        // all communications were completed with exceptions or isSuccess == false
        StringBuilder builder = new StringBuilder("All ").append(map.size()).append("communications are failed: \n");
        Throwable ex = null;
        for (Map.Entry<Future<T>, Wrapper<T>> entry: map.entrySet()) {
            builder.append("\t").append(entry.getValue().status());
            if (ex == null || !(ex instanceof IOException)) {
                ex = entry.getValue().getError();
            }
        }
        LOGGER.warn(builder.toString());
        throw new IOException(builder.toString(), ex);
    }

    private static <T> StringBuilder status(Set<Future<T>> futures, Map<Future<T>, Wrapper<T>> wrappersMap, long timeout) {
        StringBuilder builder = new StringBuilder("Valid response was not received in ").append(formatInterval(timeout));
        for (Map.Entry<Future<T>, Wrapper<T>> entry: wrappersMap.entrySet()) {
            if (futures.contains(entry.getKey())) {
                builder.append("[").append(entry.getValue().exec.info()).append("=awaiting] ");
            } else {
                builder.append("[").append(entry.getValue().exec.info()).append("=finished] ");
            }
        }
        return builder;
    }

    public interface Executable<T> {
        T call() throws IOException, InterruptedException;

        boolean isSuccess(T resp);

        String info();
    }

    public static abstract class AlwaysSuccessExecutable<T> implements Executable<T> {
        @Override
        public boolean isSuccess(T resp) {
            return true;
        }

        @Override
        public String info() {
            return "";
        }
    }

    static class Wrapper<T> {
        Executable<T> exec;
        Throwable error;

        Wrapper(Executable<T> c) {
            exec = c;
        }

        public T call() throws IOException, InterruptedException {
            LOGGER.debug("Executable started");
            return exec.call();
        }

        public Throwable getError() {
            return error;
        }

        public void setError(Throwable e) {
            this.error = e;
        }

        public String status() {
            return exec.info() + "[isSuccess=" + (error == null) +
                    " errorMsg=" + (error != null ? error.getMessage() : "no_message") + "]";
        }
    }
}
