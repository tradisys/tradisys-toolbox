package com.tradisys.games.server.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

public class FormatUtils {
    public static final String WAVES_ASSET_ID = "WAVES";
    public static final BigDecimal WAVELETS_MULT = new BigDecimal("100000000.00");
    public static final int WAVELETS_DECIMALS = 8;

    public static String formatInterval(long millis) {
        final long hr = MILLISECONDS.toHours(millis);
        millis = millis - HOURS.toMillis(hr);
        final long min = MILLISECONDS.toMinutes(millis);
        millis = millis - MINUTES.toMillis(min);
        final long sec = MILLISECONDS.toSeconds(millis);
        return String.format("%02d:%02d:%02d", hr, min, sec);
    }

    public static long toBlkMoney(BigDecimal amount) {
        return toBlkAmount(amount,WAVELETS_DECIMALS);
    }

    public static long toBlkAmount(long amount, int decimals) {
        return toBlkAmount(BigDecimal.valueOf(amount), decimals);
    }

    public static long toBlkAmount(BigDecimal amount, int decimals) {
        return amount.multiply(BigDecimal.valueOf(Math.pow(10,decimals))).longValue();
    }

    public static BigDecimal toServerMoney(long amount) {
        return toServerAmount(amount,WAVELETS_DECIMALS);
    }

    public static BigDecimal toServerAmount(long amount, Integer decimals) {
        BigDecimal amountBd = new BigDecimal(amount).setScale(12, RoundingMode.DOWN);
        return amountBd.divide(BigDecimal.valueOf(Math.pow(10,decimals)), 12, RoundingMode.DOWN);
    }

}
