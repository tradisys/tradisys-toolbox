package com.tradisys.games.server.utils;

import com.tradisys.games.server.exception.SilentExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.concurrent.Callable;

public class LambdaUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(LambdaUtils.class);

    /**
     * Allows to execute methods silently (all declared exceptions are wrapped into runtime exception)
     * @param callable task to execute in current thread
     * @param <T> the result type of execution
     * @return in case of success value that is returned by passed <code>callable</code> task
     * @throws SilentExecutionException in case of any failure of <code>callable</code> task
     * @see #silentExecutionOptResult
     */
    public static <T> T silentExecution(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Throwable ex) {
            LOGGER.debug("Silent Execution Failed", ex);
            throw new SilentExecutionException("Silent Execution Failed", ex);
        }
    }

    /**
     * Allows to execute methods silently and returns Optional result (the main difference with #silentExecution where runtime exception is thrown)
     * @param callable task to execute in current thread
     * @param <T> the result type of execution
     * @return in case of success value that is returned by passed <code>callable</code> task otherwise empty Optional object
     * @see #silentExecution
     */
    public static <T> Optional<T> silentExecutionOptResult(Callable<T> callable) {
        T result = null;
        try {
            result =  callable.call();
        } catch (Throwable ex) {
            LOGGER.debug("Silent Execution Failed", ex);
        }
        return Optional.ofNullable(result);
    }
}
