package com.tradisys.games.server.utils;

import java.math.BigDecimal;
import java.util.function.BiPredicate;

public class DefaultPredicates {

    public static final BiPredicate<BigDecimal, BigDecimal> EQUALS = new Equals();
    public static final BiPredicate<BigDecimal, BigDecimal> GREATER = new Greater();
    public static final BiPredicate<BigDecimal, BigDecimal> LESS = new Less();

    public static class Equals implements BiPredicate<BigDecimal, BigDecimal> {
        @Override
        public boolean test(BigDecimal b1, BigDecimal b2) {
            return b1 != null && b2 != null && b1.compareTo(b2) == 0;
        }
    }

    public static class Greater implements BiPredicate<BigDecimal, BigDecimal> {
        @Override
        public boolean test(BigDecimal b1, BigDecimal b2) {
            return b1 != null && b2 != null && b1.compareTo(b2) > 0;
        }
    }

    public static class Less implements BiPredicate<BigDecimal, BigDecimal> {
        @Override
        public boolean test(BigDecimal b1, BigDecimal b2) {
            return b1 != null && b2 != null && b1.compareTo(b2) < 0;
        }
    }
}
