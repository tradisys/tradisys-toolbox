package com.tradisys.games.server.exception;

public class SilentExecutionException extends RuntimeException {

    public SilentExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
