package com.tradisys.games.server.exception;

import java.io.IOException;

public class BlkChTimeoutException extends IOException {
    public BlkChTimeoutException(String message) {
        super(message);
    }
}
