package com.tradisys.games.server.exception;

import java.io.IOException;

/**
 * the same meaning as {@link java.lang.InterruptedException} but current exception uses as an ancestor base
 * integration exception class
 */
public class BlkChInterruptedException extends IOException {
    public BlkChInterruptedException(String message) {
        super(message);
    }

    public BlkChInterruptedException(String message, Throwable cause) {
        super(message, cause);
    }
}
