package com.tradisys.games.server.integration;

import com.wavesplatform.wavesj.*;
import com.wavesplatform.wavesj.transactions.DataTransaction;
import com.wavesplatform.wavesj.transactions.TransferTransactionV2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TransactionsFactory {

    public interface Proofs {
        List<ByteString> makeProofs(Transaction tx);
    }

    private static class ProofsChain implements Proofs {
        private List<Proofs> makers;

        private ProofsChain(List<Proofs> makers) {
            this.makers = makers;
        }

        public List<ByteString> makeProofs(Transaction tx) {
            List<ByteString> proofs = new ArrayList<>();
            for (Proofs maker: makers) {
                List<ByteString> proof = maker.makeProofs(tx);
                proofs.add(proof.get(0));
            }
            return proofs;
        }
    }

    public static class ProofsBuilder {
        private List<Proofs> makers = new ArrayList<>();

        public static ProofsBuilder instance() {
            return new ProofsBuilder();
        }

        public static Proofs signProof(PrivateKeyAccount fromAcc) {
            return new SignProof(fromAcc);
        }

        public ProofsBuilder withSign(PrivateKeyAccount fromAcc) {
            makers.add(new SignProof(fromAcc));
            return this;
        }

        public ProofsBuilder withValue(String value) {
            makers.add(new ValueProof(value));
            return this;
        }

        public Proofs build() {
            return new ProofsChain(makers);
        }
    }

    private static class SignProof implements Proofs {
        private PrivateKeyAccount fromAcc;

        private SignProof(PrivateKeyAccount fromAcc) {
            this.fromAcc = fromAcc;
        }

        @Override
        public List<ByteString> makeProofs(Transaction tx) {
            ByteString serverSign = new ByteString(fromAcc.sign(tx));
            return Collections.singletonList(serverSign);
        }
    }

    private static class ValueProof implements Proofs {
        private String val;

        private ValueProof(String val) {
            this.val = val;
        }

        @Override
        public List<ByteString> makeProofs(Transaction tx) {
            ByteString bVal = new ByteString(val);
            return Collections.singletonList(bVal);
        }
    }

    public static DataTransaction makeDataTx(PublicKeyAccount sender, Collection<DataEntry<?>> data, long fee, Proofs proofsMaker) {
        long timestamp = System.currentTimeMillis();
        DataTransaction tx = new DataTransaction(sender, data, fee, timestamp, Collections.emptyList());
        if (proofsMaker != null) {
            List<ByteString> proofs = proofsMaker.makeProofs(tx);
            tx = new DataTransaction(sender, data, fee, timestamp, proofs);
        }
        return tx;
    }

    public static TransferTransactionV2 makeTransferTx(PublicKeyAccount sender, String recipient, long amount, String assetId, long fee, String feeAssetId, String attachment, Proofs proofsMaker) {
        long timestamp = System.currentTimeMillis();
        ByteString attach = attachment == null ? ByteString.EMPTY : new ByteString(attachment.getBytes());
        TransferTransactionV2 tx = new TransferTransactionV2(sender, recipient, amount, assetId,
                fee, feeAssetId, attach, timestamp, Collections.emptyList());
        if (proofsMaker != null) {
            List<ByteString> proofs = proofsMaker.makeProofs(tx);
            tx = new TransferTransactionV2(sender, recipient, amount, assetId,
                    fee, feeAssetId, attach, timestamp, proofs);
        }
        return tx;
    }

    public static TransferTransactionV2 makeTransferTx(PublicKeyAccount sender, String recipient, long amount, String assetId, long fee, String feeAssetId, byte[] attachment, Proofs proofsMaker) {
        long timestamp = System.currentTimeMillis();
        ByteString attach = attachment == null ? ByteString.EMPTY : new ByteString(attachment);
        TransferTransactionV2 tx = new TransferTransactionV2(sender, recipient, amount, assetId,
                fee, feeAssetId, attach, timestamp, Collections.emptyList());
        if (proofsMaker != null) {
            List<ByteString> proofs = proofsMaker.makeProofs(tx);
            tx = new TransferTransactionV2(sender, recipient, amount, assetId,
                    fee, feeAssetId, attach, timestamp, proofs);
        }
        return tx;
    }

    public static TransferTransactionV2 makeTransferTx(PrivateKeyAccount sender, String recipient, long amount, String assetId, long fee, String feeAssetId, byte[] attachment) {
        long timestamp = System.currentTimeMillis();
        ByteString attach = attachment == null ? ByteString.EMPTY : new ByteString(attachment);
        return new TransferTransactionV2(sender, recipient, amount, assetId,
                fee, feeAssetId, attach, timestamp);
    }
}
