package com.tradisys.games.server.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wavesplatform.wavesj.Transaction;
import com.wavesplatform.wavesj.json.WavesJsonMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class WavesResponseMapper implements ResponseMapper {

    public static final TypeReference<List<TransactionInfo>> TX_INFO_LIST = new TypeReference<List<TransactionInfo>>() {};
    public static final TypeReference<List<Transaction>> TX_LIST = new TypeReference<List<Transaction>>() {};
    public static final TypeReference<List<TransactionInfo.DataEntry>> DATA_INFO_LIST = new TypeReference<List<TransactionInfo.DataEntry>>() {};
    public static final TypeReference<BalanceInfo> BALANCE_INFO = new TypeReference<BalanceInfo>() {};

    private ObjectMapper mapper;

    public WavesResponseMapper(byte chainId) {
        mapper = new WavesJsonMapper(chainId);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public List<Transaction> mapTransactions(InputStream in) throws IOException {
        JsonNode jsonTreeRoot = mapper.readTree(in);
        // skip enclosing array and read array of transactions
        return mapper.convertValue(jsonTreeRoot.get(0), TX_LIST);
    }

    @Override
    public List<TransactionInfo.DataEntry> mapDataEntries(InputStream in) throws IOException {
        JsonNode root = mapper.readTree(in);
        return mapper.readValue(root.traverse(), DATA_INFO_LIST);
    }

    @Override
    public <T> T mapAny(InputStream in, Class<T> valueType) throws IOException {
        return mapper.readValue(in, valueType);
    }

    @Override
    public String writeValueAsString(Object value) throws IOException {
        return mapper.writeValueAsString(value);
    }

    @Override
    public int asInt(InputStream in, String key) throws IOException {
        JsonNode tree = mapper.readTree(in);
        return tree.get(key).asInt();
    }
}
