package com.tradisys.games.server.integration;

import java.io.Serializable;

public class DecompileInfo implements Serializable {
    private static final long serialVersionUID = 145736804932143464L;

    private String script;

    public String getScript() {
        return script;
    }
}
