package com.tradisys.games.server.integration;

import com.tradisys.games.server.integration.TransactionInfo.DataEntry;
import com.tradisys.games.server.HttpClientConfig;
import com.wavesplatform.wavesj.*;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Deprecated
public class NodesCluster implements NodeDecorator {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodesCluster.class);
    private final NodeDecorator[] nodes;
    private ThreadPoolExecutor executor;

    public NodesCluster(byte chainId, HttpClientConfig httpClientConfig, String ... nodeUrls) throws URISyntaxException {
        this(chainId, httpClientConfig, null, nodeUrls);
    }

    public NodesCluster(byte chainId, HttpClientConfig httpClientConfig, ResponseMapper mapper, String ... nodeUrls) throws URISyntaxException {
        this(chainId, httpClientConfig, mapper, null, nodeUrls);
    }

    public NodesCluster(byte chainId, HttpClientConfig httpClientConfig, ResponseMapper mapper,
                        CustomHeightProvider heightProvider, String ... nodeUrls)
            throws URISyntaxException {
        if (nodeUrls.length <= 0) {
            throw new IllegalStateException("Node Urls are empty");
        }

        if (mapper == null) {
            mapper = new WavesResponseMapper(chainId);
        }

        nodes = new NodeDecorator[nodeUrls.length];
        int i=0;
        for (String url: nodeUrls) {
            nodes[i++] = new WavesNodeDecorator(url, chainId, mapper, httpClientConfig, heightProvider);
        }

        executor = new ThreadPoolExecutor(10, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS, new SynchronousQueue<>());
    }

    @Override
    public Node getNode() {
        return nodes[0].getNode();
    }

    @Override
    public int getHeight() throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(NodeDecorator::getHeight));
    }

    @Override
    public int getHeightSafe() {
        int height = 0;
        try {
            height = getHeight();
        } catch (IOException ex) {
            LOGGER.warn("Could not receive height - 0 is used: {}", getNodeInfo(), ex);
        }
        return height;
    }

    @Override
    public String transfer(PrivateKeyAccount from, String recipient, long amount, long fee, String message) throws IOException {
        Transaction tx = Transactions.makeTransferTx(from, recipient, amount, null, fee, null, message);
        return send(tx);
    }

    @Override
    public String massTransfer(PrivateKeyAccount from, String assetId, Collection<Transfer> transfers, long fee, String message) throws IOException {
        Transaction tx = Transactions.makeMassTransferTx(from, assetId, transfers, fee, message);
        return send(tx);
    }

    @Override
    public String massTransferAutoFee(PrivateKeyAccount fromAcc, long amount, String assetId, String... recipients) throws IOException {
        return communicate(
                new AlwaysSuccessClusteredComm<>(
                        node -> node.massTransferAutoFee(fromAcc, amount, assetId, recipients)));
    }

    @Override
    public String massTransferAutoFee(PrivateKeyAccount fromAcc, String assetId, Collection<Transfer> transfers) throws IOException {
        return communicate(
                new AlwaysSuccessClusteredComm<>(
                        node -> node.massTransferAutoFee(fromAcc, assetId, transfers)));
    }

    @Override
    public String setScript(PrivateKeyAccount from, String script, byte chainId, long fee) throws IOException {
        Transaction tx = Transactions.makeScriptTx(from, compileScript(script), chainId, fee);

        return communicate(new ClusteredCommunication<String>() {
            @Override
            public String call(NodeDecorator node) throws IOException {
                return node.send(tx);
            }

            @Override
            public boolean isSuccess(String resp) {
                LOGGER.debug("Smart contract deployed: address={} resp={}", from.getAddress(), resp);
                return true;
            }
        });
    }

    @Override
    public long getBalance(String address) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getBalance(address)));
    }

    @Override
    public ScriptInfo getScriptInfo(String address) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getScriptInfo(address)));
    }

    @Override
    public BalanceInfo getBalanceInfo(String address) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getBalanceInfo(address)));
    }

    @Override
    public BalanceInfo getBalanceInfoSafe(String address) {
        BalanceInfo balance = BalanceInfo.EMPTY;
        try {
            balance = getBalanceInfo(address);
        } catch (IOException ex) {
            LOGGER.warn("Could not receive balance 0 is used: address={}", address, ex);
        }
        return balance;
    }

    @Override
    public Transaction getTransaction(String txId, boolean withRetry) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getTransaction(txId, withRetry)));
    }

    @Override
    public Transaction getTransaction(String txId) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getTransaction(txId)));
    }

    @Override
    public Iterable<Transaction> getAllTransactions(String address, int pageSize) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAllTransactions(address, pageSize)));
    }

    @Override
    public List<Transaction> getTransactions(String address, int pageSize, String afterTx) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getTransactions(address, pageSize, afterTx)));
    }

    @Override
    public Optional<List<DataEntry>> getAccountData(String accountAddress) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAccountData(accountAddress)));
    }

    @Override
    public Optional<DataEntry> getAccountDataByKey(String accountAddress, String key) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAccountDataByKey(accountAddress,key)));
    }

    @Override
    public Optional<DataEntry> getAccountDataByKeySilently(String accountAddress, String key) {
        try {
            return getAccountDataByKey(accountAddress, key);
        } catch (IOException ex) {
            LOGGER.warn("Muted exception because of silent mode", ex);
        }
        return Optional.empty();
    }

    @Override
    public List<DataEntry> getAccountDataByKeysList(String accountAddress, String... keys) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAccountDataByKeysList(accountAddress, keys)));
    }

    @Override
    public Map<String, Optional<DataEntry>> getAccountDataByKeysMap(String accountAddress, String... keys) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAccountDataByKeysMap(accountAddress, keys)));
    }

    @Override
    public List<DataEntry> getAccountDataByKeyRegExp(String accountAddress, String keyRegExp) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAccountDataByKeyRegExp(accountAddress, keyRegExp)));
    }

    @Override
    public String send(Transaction tx) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.send(tx)));
    }

    @Override
    public String sendSafe(Transaction tx) {
        String txId = null;
        try {
            txId = send(tx);
        } catch (Throwable ex) {
            String failedTxId = tx != null ? tx.getId().toString() : "<empty>";
            LOGGER.error("Failure in sendSafe: tx_id={}", failedTxId , ex);
        }
        return txId;
    }

    @Override
    public String sendAndWait(TxBuilder builder, long timeToWait, int retryCount) throws IOException, InterruptedException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(
                        node -> node.sendAndWait(builder, timeToWait, retryCount)));
    }

    @Override
    public String compileScript(String script) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.compileScript(script)));
    }

    @Override
    public DecompileInfo decompileScript(String base58Script) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.decompileScript(base58Script)));
    }

    @Override
    public CloseableHttpResponse sendJson(String json) throws IOException {
       return communicate(new ClusteredCommunication<CloseableHttpResponse>() {
           @Override
           public CloseableHttpResponse call(NodeDecorator node) throws IOException {
               return node.sendJson(json);
           }

           @Override
           public boolean isSuccess(CloseableHttpResponse resp) {
               // todo think about resources clean up
               return resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
           }
       });
    }

    @Override
    public BigDecimal waitMoneyOnBalance(String addressToCheck, BigDecimal initialAmount, BigDecimal amountToWait, BiPredicate<BigDecimal, BigDecimal> predicate, long timeToWait)
            throws IOException, InterruptedException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(
                        node -> node.waitMoneyOnBalance(addressToCheck, initialAmount, amountToWait, predicate, timeToWait)));
    }

    @Override
    public Transaction waitTransaction(String txId) throws IOException, InterruptedException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(node -> node.waitTransaction(txId)));
    }

    @Override
    public Transaction waitTransaction(String txId, long timeToWait) throws IOException, InterruptedException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(node -> node.waitTransaction(txId, timeToWait)));
    }

    @Override
    public Transaction waitTransactions(Set<String> txIds) throws IOException, InterruptedException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(node -> node.waitTransactions(txIds)));
    }

    @Override
    public Transaction waitTransactions(Set<String> txIds, long timeToWait) throws IOException, InterruptedException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(node -> node.waitTransactions(txIds, timeToWait)));
    }

    @Override
    public <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate) throws InterruptedException, IOException {
        return whileInState(stateSupplier, loopContinuePredicate, o -> {});
    }

    @Override
    public <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate, Consumer<Optional<T>> timeoutCallback) throws InterruptedException, IOException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(node -> node.whileInState(stateSupplier, loopContinuePredicate, timeoutCallback)));
    }

    @Override
    public <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate, int confirmations, long timeout, Consumer<Optional<T>> timeoutCallback) throws InterruptedException, IOException {
        return communicateInterrupted(
                new InterruptedAlwaysSuccessClusteredComm<>(node -> node.whileInState(stateSupplier, loopContinuePredicate, confirmations, timeout, timeoutCallback)));
    }

    @Override
    public void sleepBlocks(int blocks) throws IOException, InterruptedException {
        communicateInterrupted(new InterruptedAlwaysSuccessClusteredComm<>(node -> {
            node.sleepBlocks(blocks);
            return true;
        }));
    }

    @Override
    public void sleepBlocks(int blocks, long maxWaitingTime) throws IOException, InterruptedException {
        communicateInterrupted(new InterruptedAlwaysSuccessClusteredComm<>(node -> {
            node.sleepBlocks(blocks, maxWaitingTime);
            return true;
        }));
    }

    @Override
    public void waitHeight(int heightToWait) throws IOException, InterruptedException {
        communicateInterrupted(new InterruptedAlwaysSuccessClusteredComm<>(node -> {
            node.waitHeight(heightToWait);
            return true;
        }));
    }

    @Override
    public void waitHeight(int heightToWait, long maxWaitingTime) throws IOException, InterruptedException {
        communicateInterrupted(new InterruptedAlwaysSuccessClusteredComm<>(node -> {
            node.waitHeight(heightToWait, maxWaitingTime);
            return true;
        }));
    }

    @Override
    public void logStatus() {
        if (LOGGER.isDebugEnabled() ) {
            LOGGER.debug("NodeCluster thread pool info: activeCount={} corePoolSize={} largestPoolSize={} maximumPoolSize={} completedTaskCount={}",
                    executor.getActiveCount(), executor.getCorePoolSize(),
                    executor.getLargestPoolSize(), executor.getMaximumPoolSize(), executor.getCompletedTaskCount());
        }
        for (NodeDecorator node: nodes) {
            node.logStatus();
        }
    }

    @Override
    public String getNodeInfo() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("NodeCluster info:");

        int i = 0;
        for (NodeDecorator node: nodes) {
            builder.append("    node[").append(i++).append("]: ").append(node.getNodeInfo());
        }

        return builder.toString();
    }

    private <T> T communicateInterrupted(InterruptedClusteredCommunication<T> comm) throws IOException, InterruptedException {
        if (nodes.length > 1) {
            return clusterCommunicateNew(comm);
        } else {
            return comm.call(nodes[0]);
        }
    }

    public AssetBalanceInfo getAssetBalance(String address, String assetId) throws IOException {
        return communicate(new AlwaysSuccessClusteredComm<>(node -> node.getAssetBalance(address,assetId)));
    }

    private <T> T communicate(ClusteredCommunication<T> comm) throws IOException {
        if (nodes.length > 1) {
            return clusterCommunicateNew(comm);
        } else {
            return comm.call(nodes[0]);
        }
    }

    // todo refactor to use ExecutingAlgs.firstSuccessTask
    private <T> T clusterCommunicateNew(InterruptedClusteredCommunication<T> comm) throws IOException {
        Set<Future<T>> tasks = new HashSet<>(nodes.length + 1);
        CompletionService<T> completionService = new ExecutorCompletionService<>(executor);
        Map<Future<T>, Wrapper<T>> map = new HashMap<>();

        for (int i = 0; i < nodes.length; i++) {
            final Wrapper<T> w = new Wrapper<>(nodes[i], comm);
            Future<T> f = completionService.submit(w::call);
            tasks.add(f);
            map.put(f, w);
        }

        try {
            long timeout = 500;
            int ticks = 0;
            while (tasks.size() > 0) {
                Future<T> f = completionService.poll();

                if (f != null) {
                    Wrapper<T> w = map.get(f);
                    try {
                        T resp = f.get(5, TimeUnit.SECONDS);
                        if (comm.isSuccess(resp)) {
                            return resp; // remaining tasks will be cancelled in finally
                        }
                    } catch (ExecutionException ex) {
                        LOGGER.trace("Communication failed: node={}", w.getNode().getNodeInfo(), ex);
                        w.setError(ex.getCause());
                    } catch (TimeoutException ex) {
                        LOGGER.error("Impossible situation was tracked just in case: node={}", w.getNode().getNodeInfo(), ex);
                        w.setError(ex);
                    } finally {
                        // response received
                        tasks.remove(f);
                    }
                } else if (ticks * timeout > 30 * 1000) {
                    // valid response was not received in 30 sec - need to stop
                    StringBuilder builder = new StringBuilder("Valid response was not received in 30 sec: ");
                    for (Map.Entry<Future<T>, Wrapper<T>> entry: map.entrySet()) {
                        if (tasks.contains(entry.getKey())) {
                            builder.append("[").append(entry.getValue().getNode().getNodeInfo()).append("=awaiting] ");
                        } else {
                            builder.append("[").append(entry.getValue().getNode().getNodeInfo()).append("=finished] ");
                        }
                    }
                    LOGGER.debug(builder.toString());
                    throw new IOException(builder.toString());
                }

                Thread.sleep(timeout);
                ticks++;
            }
        } catch (InterruptedException ex) {
            // stop execution - clean up is done in finally block
            LOGGER.warn("Request to terminate thread was sent - all communications were stopped", ex);
            throw new IOException("Request to terminate thread was sent - all communications were stopped");
        } finally {
            tasks.forEach(t -> t.cancel(true));
        }

        // all communications were completed with exceptions or isSuccess == false
        StringBuilder builder = new StringBuilder("All communications are failed: \n");
        Throwable ex = null;
        for (Map.Entry<Future<T>, Wrapper<T>> entry: map.entrySet()) {
            builder.append("\t").append(entry.getValue().status());
            if (ex == null || !(ex instanceof IOException)) {
                ex = entry.getValue().getError();
            }
        }
        LOGGER.debug(builder.toString());
        throw new IOException(builder.toString(), ex);
    }

    interface InterruptedClusteredCommunication<T> {
        T call(NodeDecorator node) throws IOException, InterruptedException;

        boolean isSuccess(T resp);
    }

    interface ClusteredCommunication<T> extends InterruptedClusteredCommunication<T> {
        T call(NodeDecorator node) throws IOException;
    }

    static class InterruptedAlwaysSuccessClusteredComm<T> implements InterruptedClusteredCommunication<T> {
        InterruptedAlwaysSuccessAdapter<T> adapter;

        InterruptedAlwaysSuccessClusteredComm(InterruptedAlwaysSuccessAdapter<T> adapter) {
            this.adapter = adapter;
        }

        @Override
        public T call(NodeDecorator node) throws IOException, InterruptedException {
            return adapter.call(node);
        }

        public boolean isSuccess(T resp) {
            return true;
        }
    }

    static class AlwaysSuccessClusteredComm<T> implements ClusteredCommunication<T> {
        AlwaysSuccessAdapter<T> adapter;

        AlwaysSuccessClusteredComm(AlwaysSuccessAdapter<T> adapter) {
            this.adapter = adapter;
        }

        @Override
        public T call(NodeDecorator node) throws IOException {
            return adapter.call(node);
        }

        @Override
        public boolean isSuccess(T resp) {
            return true;
        }
    }

    interface InterruptedAlwaysSuccessAdapter<T> {
        T call(NodeDecorator node) throws IOException, InterruptedException;
    }

    interface AlwaysSuccessAdapter<T> extends InterruptedAlwaysSuccessAdapter<T> {
        T call(NodeDecorator node) throws IOException;
    }

    static class Wrapper<T> {
        NodeDecorator node;
        InterruptedClusteredCommunication<T> comm;
        Throwable error;

        Wrapper(NodeDecorator n, InterruptedClusteredCommunication<T> c) {
            node = n;
            comm = c;
        }

        public T call() throws IOException, InterruptedException {
            LOGGER.info("Communication started with {}", node.getNodeInfo());
            return comm.call(node);
        }

        public Throwable getError() {
            return error;
        }

        public void setError(Throwable e) {
            this.error = e;
        }

        public NodeDecorator getNode() {
            return node;
        }

        public String status() {
            return node.getNodeInfo() + "[isSuccess=" + (error == null) +
                    " errorMsg=" + (error != null ? error.getMessage() : "no_message") + "]";
        }
    }
}
