package com.tradisys.games.server.integration;

public class ScriptInfo {

    private String address;
    private Integer complexity;
    private Integer extraFee;
    private String script;
    private String scriptText;

    public static final ScriptInfo EMPTY = new ScriptInfo();

    public boolean isEmpty() {
        return address == null;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getComplexity() {
        return complexity;
    }

    public void setComplexity(Integer complexity) {
        this.complexity = complexity;
    }

    public Integer getExtraFee() {
        return extraFee;
    }

    public void setExtraFee(Integer extraFee) {
        this.extraFee = extraFee;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getScriptText() {
        return scriptText;
    }

    public void setScriptText(String scriptText) {
        this.scriptText = scriptText;
    }
}
