package com.tradisys.games.server.integration;

import com.tradisys.games.server.integration.TransactionInfo.DataEntry;
import com.tradisys.games.server.utils.DefaultPredicates;
import com.wavesplatform.wavesj.*;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface NodeDecorator {
    byte MAINNET = Account.MAINNET;
    byte TESTNET = Account.TESTNET;
    byte TRADISYSNET = 'D';

    Node getNode();

    int getHeight() throws IOException;

    int getHeightSafe();

    String transfer(PrivateKeyAccount from, String recipient, long amount, long fee, String message) throws IOException;

    String massTransfer(PrivateKeyAccount from, String assetId, Collection<Transfer> transfers,
                        long fee, String message) throws IOException;

    String massTransferAutoFee(PrivateKeyAccount fromAcc, long amount, String assetId, String ... recipients) throws IOException;

    String massTransferAutoFee(PrivateKeyAccount fromAcc, String assetId, Collection<Transfer> transfers) throws IOException;

    String setScript(PrivateKeyAccount from, String script, byte chainId, long fee) throws IOException;

    /**
     * Retrieves regular balance from account
     * @param address account address
     * @return Regular Balance
     * @throws IOException - in case of any network error. Use java.lang.Throwable#getMessage() to receive all details what happened.
     * @deprecated use {@link #getBalanceInfo(java.lang.String)}
     */
    @Deprecated
    long getBalance(String address) throws IOException;

    BalanceInfo getBalanceInfo(String address) throws IOException;

    BalanceInfo getBalanceInfoSafe(String address);

    ScriptInfo getScriptInfo(String address) throws IOException;

    AssetBalanceInfo getAssetBalance(String address, String assetId) throws IOException;

    Transaction getTransaction(String txId, boolean withRetry) throws IOException;

    Transaction getTransaction(String txId) throws IOException;

    String send(Transaction tx) throws IOException;

    String sendSafe(Transaction tx);

    String sendAndWait(TxBuilder builder, long timeToWait, int retryCount) throws IOException, InterruptedException;

    CloseableHttpResponse sendJson(String json) throws IOException;

    String compileScript(String script) throws IOException;

    DecompileInfo decompileScript(String base58Script) throws IOException;

    Iterable<Transaction> getAllTransactions(String address, int pageSize) throws IOException;

    List<Transaction> getTransactions(String address, int pageSize, String afterTx) throws IOException;

    Optional<List<DataEntry>> getAccountData(String accountAddress) throws IOException;

    Optional<DataEntry> getAccountDataByKey(String accountAddress, String key) throws IOException;

    List<DataEntry> getAccountDataByKeysList(String accountAddress, String ... keys) throws IOException;

    Map<String, Optional<DataEntry>> getAccountDataByKeysMap(String accountAddress, String ... keys) throws IOException;

    List<DataEntry> getAccountDataByKeyRegExp(String accountAddress, String keyRegExp) throws IOException;

    Optional<DataEntry> getAccountDataByKeySilently(String accountAddress, String key);

    /**
     * Wait specified amount on account address.<br><br>
     * It is unsafe to use "==" operator because someone else can transfer funds to the same account.
     * But there are cases where "==" is useful (integration tests).<br>
     * That why <code>predicate</code> parameter is used where
     * <ul>
     *     <li><b>first arg</b> is an actual amount on account</li>
     *     <li><b>second arg</b> is <code>initialAmount + amountToWait</code></li>
     * </ul>
     * You can use existing {@link DefaultPredicates} for predicate parameter.
     *
     * @param addressToCheck account's address
     * @param initialAmount amount before funds transfer
     * @param amountToWait amount which was transferred to account
     * @param predicate predicate for comparison. See {@link DefaultPredicates} class.
     *                  <ul><li><b>first arg</b> is an actual amount on account</li>
     *                  <li><b>second arg</b> is <code>initialAmount + amountToWait</code></li></ul>
     * @param timeToWait timeout
     *
     * @return actual account balance
     * @throws IOException in case of errors with Node communications
     */
    BigDecimal waitMoneyOnBalance(String addressToCheck, BigDecimal initialAmount, BigDecimal amountToWait, BiPredicate<BigDecimal, BigDecimal> predicate, long timeToWait)
            throws IOException, InterruptedException;

    Transaction waitTransaction(String txId) throws IOException, InterruptedException;

    Transaction waitTransaction(String txId, long timeToWait) throws IOException, InterruptedException;

    Transaction waitTransactions(Set<String> txIds) throws IOException, InterruptedException;

    Transaction waitTransactions(Set<String> txIds, long timeToWait) throws IOException, InterruptedException;

    <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate) throws InterruptedException, IOException;

    <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate,
                       Consumer<Optional<T>> timeoutCallback) throws InterruptedException, IOException;

    <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate, int confirmations,
                                 long timeout, Consumer<Optional<T>> timeoutCallback) throws InterruptedException, IOException;

    void sleepBlocks(int blocks) throws IOException, InterruptedException;

    void sleepBlocks(int blocks, long maxWaitingTime) throws IOException, InterruptedException;

    void waitHeight(int heightToWait) throws IOException, InterruptedException;

    void waitHeight(int heightToWait, long maxWaitingTime) throws IOException, InterruptedException;

    void logStatus();

    String getNodeInfo();

    interface TxBuilder {
        Transaction tx();
    }

    interface StateSupplier<T> {
        T get() throws Exception;
    }
}
