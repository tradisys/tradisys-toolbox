package com.tradisys.games.server.integration;

public interface CustomHeightProvider {
    int getHeight();
}