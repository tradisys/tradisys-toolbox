package com.tradisys.games.server.integration;

import com.wavesplatform.wavesj.Transaction;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface ResponseMapper {

    List<Transaction> mapTransactions(InputStream in) throws IOException;

    <T> T mapAny(InputStream in, Class<T> valueType) throws IOException;

    String writeValueAsString(Object value) throws IOException;

    int asInt(InputStream in, String key) throws IOException;

    List<TransactionInfo.DataEntry> mapDataEntries(InputStream in) throws IOException;
}
