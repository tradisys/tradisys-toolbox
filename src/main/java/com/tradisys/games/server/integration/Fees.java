package com.tradisys.games.server.integration;

import com.wavesplatform.wavesj.Asset;

import java.math.BigDecimal;

public class Fees {
    public static class DECIMALS {
        public static final BigDecimal ISSUE_FEE = new BigDecimal("1.0");
        public static final BigDecimal TRANSFER_FEE = new BigDecimal("0.001");
        public static final BigDecimal SCRIPT_SET_FEE = new BigDecimal("0.01");
        public static final BigDecimal SCRIPT_TX_FEE = new BigDecimal("0.005");
        public static final BigDecimal DATA_FEE = new BigDecimal("0.001");
    }

    public static class WAVES {
        public static final long ISSUE_FEE = Asset.toWavelets(DECIMALS.ISSUE_FEE);
        public static final long TRANSFER_FEE = Asset.toWavelets(DECIMALS.TRANSFER_FEE);
        public static final long SCRIPT_SET_FEE = Asset.toWavelets(DECIMALS.SCRIPT_SET_FEE);
        public static final long SCRIPT_TX_FEE = Asset.toWavelets(DECIMALS.SCRIPT_TX_FEE);
        public static final long DATA_FEE = Asset.toWavelets(DECIMALS.DATA_FEE);
    }
}
