package com.tradisys.games.server.integration;

import java.io.Serializable;

public class AssetBalanceInfo implements Serializable {

    private String assetId;
    private long balance;
    private String address;
    public static final AssetBalanceInfo EMPTY = new AssetBalanceInfo();

    public boolean isEmpty() {
        return address == null && assetId == null;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
