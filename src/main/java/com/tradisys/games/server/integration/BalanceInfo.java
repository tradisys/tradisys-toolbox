package com.tradisys.games.server.integration;

import java.io.Serializable;

public class BalanceInfo implements Serializable {

    private static final long serialVersionUID = -444936814932443444L;

    private long regular;
    private long generating;
    private long available;
    private long effective;
    private String address;

    public static final BalanceInfo EMPTY = new BalanceInfo();

    public BalanceInfo() {
    }

    public long getRegular() {
        return regular;
    }

    public long getGenerating() {
        return generating;
    }

    public long getAvailable() {
        return available;
    }

    public long getEffective() {
        return effective;
    }

    public String getAddress() {
        return address;
    }

    public boolean isEmpty() {
        return address == null;
    }

    @Override
    public String toString() {
        return address +
               "[regular=" + regular +
               " generating=" + generating +
               " available=" + available +
               " effective=" + effective + "]";
    }
}
