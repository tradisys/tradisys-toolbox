package com.tradisys.games.server.integration;

import com.tradisys.games.server.integration.TransactionInfo.DataEntry;
import com.tradisys.games.server.HttpClientConfig;
import com.tradisys.games.server.exception.BlkChTimeoutException;
import com.tradisys.games.server.utils.ExecutingAlgs;
import com.tradisys.games.server.utils.ExecutingAlgs.AlwaysSuccessExecutable;
import com.tradisys.games.server.utils.HttpClientUtils;
import com.wavesplatform.wavesj.*;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.tradisys.games.server.integration.Fees.DECIMALS.TRANSFER_FEE;
import static com.tradisys.games.server.utils.FormatUtils.formatInterval;
import static com.tradisys.games.server.utils.FormatUtils.toServerMoney;

public class WavesNodeDecorator implements NodeDecorator {

    private static final long   DEFAULT_AVG_BLOCK_DELAY = 60 * 1000L;
    private static final long   DEFAULT_WAIT_TIMEOUT = 60 * 1000L;
    private static final int    DEFAULT_RETRIES_COUNT   = 5;
    private static final int    DEFAULT_WHILEINSTATE_CONFIRMS   = 5;

    protected static final Logger LOGGER = LoggerFactory.getLogger(WavesNodeDecorator.class);

    private Node node;
    private String nodeUrl;
    private ResponseMapper mapper;
    private CloseableHttpClient httpClient;
    private PoolingHttpClientConnectionManager connMng;
    private CustomHeightProvider customHeightProvider;
    private long avgBlockDelay = DEFAULT_AVG_BLOCK_DELAY;
    private int retriesCount = DEFAULT_RETRIES_COUNT;
    private long defaultWaitTimeout;
    private int defaultWhileInStateConfirms;

    public WavesNodeDecorator(@Nonnull String nodeUrl, byte chainId, @Nonnull HttpClientConfig httpClientConfig)
            throws URISyntaxException {
        this(nodeUrl, chainId, null, httpClientConfig);
    }

    public WavesNodeDecorator(@Nonnull String nodeUrl, byte chainId, @Nullable ResponseMapper mapper, @Nonnull HttpClientConfig httpClientConfig)
            throws URISyntaxException {
        this(nodeUrl, chainId, mapper, httpClientConfig, null);
    }

    public WavesNodeDecorator(@Nonnull String nodeUrl, byte chainId, @Nullable ResponseMapper mapper,
                              @Nonnull HttpClientConfig httpClientConfig, @Nullable CustomHeightProvider heightProvider)
            throws URISyntaxException {
        this(nodeUrl, chainId, mapper, httpClientConfig, heightProvider,
                DEFAULT_AVG_BLOCK_DELAY, DEFAULT_RETRIES_COUNT, DEFAULT_WAIT_TIMEOUT, DEFAULT_WHILEINSTATE_CONFIRMS);
    }

    public WavesNodeDecorator(@Nonnull String nodeUrl, byte chainId, @Nullable ResponseMapper mapper,
                              @Nonnull HttpClientConfig httpClientConfig, @Nullable CustomHeightProvider heightProvider,
                              long avgBlockDelay, int retriesCount,
                              long defaultWaitTimeout, int defaultWhileInStateConfirms)
            throws URISyntaxException {
        Objects.requireNonNull(httpClientConfig);
        this.nodeUrl = nodeUrl;
        this.mapper = mapper != null ? mapper : new WavesResponseMapper(chainId);
        this.httpClient = initHttpClient(httpClientConfig);
        this.node = new Node(nodeUrl, chainId, httpClient);
        this.customHeightProvider = heightProvider;
        this.avgBlockDelay = avgBlockDelay > 0 ? avgBlockDelay : DEFAULT_AVG_BLOCK_DELAY;
        this.retriesCount = retriesCount >= 0 ? retriesCount : DEFAULT_RETRIES_COUNT;
        this.defaultWaitTimeout = defaultWaitTimeout >= 0 ? defaultWaitTimeout : DEFAULT_WAIT_TIMEOUT;
        this.defaultWhileInStateConfirms = defaultWhileInStateConfirms >= 0 ? defaultWhileInStateConfirms : DEFAULT_WHILEINSTATE_CONFIRMS;
    }

    private CloseableHttpClient initHttpClient(HttpClientConfig httpClientConfig) {
        LOGGER.info("Initialize http client for node={}", nodeUrl);
        LOGGER.info("httpClientConfig.poolMaxTotal={}", httpClientConfig.getPoolMaxTotal());
        LOGGER.info("httpClientConfig.poolDefaultMaxPerRoute={}", httpClientConfig.getPoolMaxPerBlkChtRoute());
        LOGGER.info("httpClientConfig.connectTimeout={}", httpClientConfig.getConnectTimeout());
        LOGGER.info("httpClientConfig.socketTimeout={}", httpClientConfig.getSocketTimeout());
        LOGGER.info("httpClientConfig.connectionRequestTimeout={}", httpClientConfig.getConnectionRequestTimeout());

        connMng = new PoolingHttpClientConnectionManager(120*1000, TimeUnit.MILLISECONDS);
        connMng.setMaxTotal(httpClientConfig.getPoolMaxTotal());
        connMng.setDefaultMaxPerRoute(httpClientConfig.getPoolDefaultMaxPerRoute());
        connMng.setValidateAfterInactivity(150*1000);

        /*HttpHost wavesHost = HttpHost.create(configuration.getNodeUrl() + ":443");
        cm.setMaxPerRoute(new HttpRoute(wavesHost), httpClientConfig.getPoolMaxPerBlkChtRoute());
        logger.info("httpClientConfig.poolMaxPerBlkChtRoute: host={} maxPerRoute={}", wavesHost.toHostString(),
                httpClientConfig.getPoolMaxPerBlkChtRoute());*/


        return HttpClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setConnectTimeout(httpClientConfig.getConnectTimeout())
                        .setSocketTimeout(httpClientConfig.getSocketTimeout())
                        .setConnectionRequestTimeout(httpClientConfig.getConnectionRequestTimeout())
                        .setCookieSpec(CookieSpecs.STANDARD)
                        .build())
                .setConnectionManager(connMng)
                .build();
    }

    @Override
    public Node getNode() {
        return node;
    }

    @Override
    public int getHeight() throws IOException {
        if (customHeightProvider != null) {
            return customHeightProvider.getHeight();
        }

        return communicate(true, new BaseCommunication<Integer>() {
            @Override
            public Integer call() throws IOException {
                return node.getHeight();
            }

            @Override
            public void onRetry(@Nullable Integer response, int retryNum) {
                LOGGER.trace("RETRY to get height: retryAttempt={} nodeUrl={}", retryNum, nodeUrl);
            }
        });
    }

    @Override
    public int getHeightSafe() {
        int height = 0;
        try {
            height = getHeight();
        } catch (IOException ex) {
            LOGGER.warn("Could not receive height - 0 is used: {}", getNodeInfo(), ex);
        }
        return height;
    }

    @Override
    public String transfer(PrivateKeyAccount from, String recipient, long amount, long fee, String message) throws IOException {
        return communicate(true, new BaseCommunication<String>() {
            @Override
            public String call() throws IOException {
                return node.transfer(from, recipient, amount, fee, message);
            }

            @Override
            public void onRetry(@Nullable String response, int retryNum) {
                LOGGER.trace("RETRY to send transfer: retryAttempt={} nodeUrl={} fromAddress={} toAddress={}",
                        retryNum, nodeUrl, from.getAddress(), recipient);
            }
        });
    }

    @Override
    public String massTransfer(PrivateKeyAccount from, String assetId, Collection<Transfer> transfers,
                               long fee, String message) throws IOException {
        return communicate(true, new BaseCommunication<String>() {
            @Override
            public String call() throws IOException {
                return node.massTransfer(from, assetId, transfers, fee, message);
            }

            @Override
            public void onRetry(@Nullable String response, int retryNum) {
                StringBuilder toAddress = new StringBuilder(100);
                for (Transfer tr: transfers) {
                    toAddress.append(tr.getRecipient()).append(" - ").append(tr.getAmount()).append("; ");
                }
                LOGGER.trace("Retry to send mass transfer: retryAttempt={} nodeUrl={} fromAddress={} toAddresses={}",
                        retryNum, nodeUrl, from.getAddress(), toAddress.toString());
            }
        });
    }

    @Override
    public String massTransferAutoFee(PrivateKeyAccount fromAcc, long amount, String assetId, String... recipients) throws IOException {
        List<Transfer> transfers = new ArrayList<>(recipients.length + 1);
        for (String recipient: recipients) {
            transfers.add(new Transfer(recipient, amount));
        }
        return massTransferAutoFee(fromAcc, assetId, transfers);
    }

    @Override
    public String massTransferAutoFee(PrivateKeyAccount fromAcc, String assetId, Collection<Transfer> transfers) throws IOException {
        BigDecimal mtxFee = TRANSFER_FEE.multiply(new BigDecimal(1 + (transfers.size() + 1) / 2));
        return massTransfer(fromAcc, assetId, transfers, Asset.toWavelets(mtxFee), "");
    }

    @Override
    public String setScript(PrivateKeyAccount from, String script, byte chainId, long fee) throws IOException {
        return communicate(true, new BaseCommunication<String>() {
            @Override
            public String call() throws IOException {
                return node.setScript(from, node.compileScript(script), chainId, fee);
            }

            @Override
            public void onRetry(@Nullable String response, int retryNum) {
                LOGGER.trace("RETRY to set script: retryAttempt={} nodeUrl={} address={}", retryNum, nodeUrl, from.getAddress());
            }
        });
    }

    @Override
    public String compileScript(String script) throws IOException {
        return communicate(true, new BaseCommunication<String>() {
            @Override
            public String call() throws IOException {
                return node.compileScript(script);
            }

            @Override
            public void onRetry(@Nullable String response, int retryNum) {
                LOGGER.trace("RETRY to compile script: retryAttempt={} nodeUrl={}", retryNum, nodeUrl);
            }
        });
    }

    @Override
    public DecompileInfo decompileScript(String base58Script) throws IOException {
        Objects.requireNonNull(base58Script);

        try (CloseableHttpResponse resp = postJson(base58Script, nodeUrl + "/utils/script/decompile")) {
            return mapper.mapAny(resp.getEntity().getContent(), DecompileInfo.class);
        }
    }

    @Override
    public long getBalance(String address) throws IOException {
        return communicate(true, new BaseCommunication<Long>() {
            @Override
            public Long call() throws IOException {
                return node.getBalance(address);
            }

            @Override
            public void onRetry(@Nullable Long response, int retryNum) {
                LOGGER.trace("RETRY to get balance: retryAttempt={} nodeUrl={} address={}", retryNum, nodeUrl, address);
            }
        });
    }

    @Override
    public BalanceInfo getBalanceInfo(String address) throws IOException {
        Objects.requireNonNull(address);
        final HttpGet req = new HttpGet(nodeUrl + "/addresses/balance/details/" + address);
        return communicate(true, new BaseCommunication<BalanceInfo>() {
            @Override
            public BalanceInfo call() throws IOException {
                try (CloseableHttpResponse resp = httpClient.execute(req)) {
                    if (resp.getStatusLine() != null
                            && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                            && resp.getEntity() != null) {
                        return mapper.mapAny(resp.getEntity().getContent(), BalanceInfo.class);
                    }
                }
                return BalanceInfo.EMPTY;
            }

            @Override
            public boolean isSuccess(@Nonnull BalanceInfo response) {
                return !response.isEmpty();
            }

            @Override
            public void onRetry(@Nullable BalanceInfo response, int retryNum) {
                LOGGER.trace("RETRY to obtain balance info: retryAttempt={} nodeUrl={} address={}",
                        retryNum, nodeUrl, address);
            }
        });
    }

    public AssetBalanceInfo getAssetBalance(String address, String assetId) throws IOException{
        Objects.requireNonNull(address);
        Objects.requireNonNull(assetId);
        final HttpGet req = new HttpGet(nodeUrl + "/assets/balance/" + address+ "/" + assetId);
        return communicate(true, new BaseCommunication<AssetBalanceInfo>() {
            @Override
            public AssetBalanceInfo call() throws IOException {
                try (CloseableHttpResponse resp = httpClient.execute(req)) {
                    if (resp.getStatusLine() != null
                            && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                            && resp.getEntity() != null) {
                        return mapper.mapAny(resp.getEntity().getContent(), AssetBalanceInfo.class);
                    }
                }
                return AssetBalanceInfo.EMPTY;
            }

            @Override
            public boolean isSuccess(@Nonnull AssetBalanceInfo response) {
                return !response.isEmpty();
            }

            @Override
            public void onRetry(@Nullable AssetBalanceInfo response, int retryNum) {
                LOGGER.trace("RETRY to obtain balance info: retryAttempt={} nodeUrl={} address={}",
                        retryNum, nodeUrl, address);
            }
        });
    }

    @Override
    public BalanceInfo getBalanceInfoSafe(String address) {
        BalanceInfo balance = BalanceInfo.EMPTY;
        try {
            balance = getBalanceInfo(address);
        } catch (IOException ex) {
            LOGGER.warn("Could not receive balance 0 is used: address={}", address, ex);
        }
        return balance;
    }

    @Override
    public ScriptInfo getScriptInfo(String address) throws IOException {
        Objects.requireNonNull(address);
        final HttpGet req = new HttpGet(nodeUrl + "/addresses/scriptInfo/" + address);
        return communicate(true, new BaseCommunication<ScriptInfo>() {
            @Override
            public ScriptInfo call() throws IOException {
                try (CloseableHttpResponse resp = httpClient.execute(req)) {
                    if (resp.getStatusLine() != null
                            && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                            && resp.getEntity() != null) {
                        return mapper.mapAny(resp.getEntity().getContent(), ScriptInfo.class);
                    }
                }
                return ScriptInfo.EMPTY;
            }

            @Override
            public boolean isSuccess(@Nonnull ScriptInfo response) {
                return !response.isEmpty();
            }

            @Override
            public void onRetry(@Nullable ScriptInfo response, int retryNum) {
                LOGGER.trace("RETRY to obtain balance info: retryAttempt={} nodeUrl={} address={}",
                        retryNum, nodeUrl, address);
            }
        });
    }

    @Override
    public Transaction getTransaction(String txId, boolean withRetry) throws IOException {
        return communicate(withRetry, new BaseCommunication<Transaction>() {
            @Override
            public Transaction call() throws IOException {
                return node.getTransaction(txId);
            }

            @Override
            public void onRetry(@Nullable Transaction response, int retryNum) {
                LOGGER.trace("RETRY to get transaction: retryAttempt={} nodeUrl={} txId={}", retryNum, nodeUrl, txId);
            }
        });
    }

    @Override
    public Transaction getTransaction(String txId) throws IOException {
        return getTransaction(txId, true);
    }

    /**
     * <b>WARNING</b><br>
     * <b>NEVER USE THIS ALGORITHM FOR TRANSFERS</b>, see explanation below<br>
     *
     * The following algorithm is used:<br>
     * send transaction to block chain and then try to pull it by id from block chain<br>
     * if response is not received in <b><code>timeToWait</code></b> then a new transaction with the same data will be posted
     * and method will try to pull info for two transactions.<br>
     * if information for at least one transaction is not received again in <b><code>timeToWait</code></b> then the third transaction
     * will be posted and etc... <br>
     * Such resending will be continued <b><code>retryCount</code></b> times.<br>
     * <br>
     * As a result <b><code>retryCount</code></b> transactions can be posted to block chain and <b>max timeout</b> will be <b><code>retryCount x timeToWait</code></b><br>
     * Such behaviour can be acceptable only by smart contract which is strictly regulates sequence for data transactions otherwise
     * you can post more than one transaction.<br>
     *
     * @param builder builder to create a new transaction on retry operation
     * @param timeToWait how much time to scan transactions in block chain on each retry iteration
     * @param retryCount how many retries to do
     * @return id of success transaction
     * @throws IOException in case of communication errors
     */
    @Override
    public String sendAndWait(TxBuilder builder, long timeToWait, int retryCount) throws IOException, InterruptedException {
        List<String> ids = new ArrayList<>(retryCount + 1);

        int retry = 0;
        Exception origEx = null;
        do {
            if (retry > 0) {
                LOGGER.trace("Retry to send and wait transaction: {} from {}", retry, retryCount);
            }

            Transaction tx = builder.tx();
            byte txType = tx.getType();
            if (txType != TransactionInfo.DATA
                    && txType != TransactionInfo.SET_SCRIPT
                    && txType != TransactionInfo.TRANSFER) {
                throw new IllegalArgumentException("Only Data and Set Script transactions are supported");
            }

            try {
                String txId = sendSafe(tx);
                if (txId != null) {
                    ids.add(txId);
                }

                if (ids.isEmpty()) {
                    // first transaction also failed
                    Thread.sleep(5 * 1000);
                } else if (ids.size() == 1) {
                    waitTransaction(ids.get(0), timeToWait);
                    return ids.get(0);
                } else {
                    List<WaitTransactionExecutable> waitingTasks = new ArrayList<>();
                    for (String id : ids) {
                        WaitTransactionExecutable task = new WaitTransactionExecutable(id, timeToWait);
                        waitingTasks.add(task);
                    }
                    return ExecutingAlgs.firstSuccessTask(waitingTasks, timeToWait);
                }
            } catch (IOException ex) {
                origEx = ex;
                LOGGER.warn("Problem during sending and waiting transaction: tx_info={} error_msg={}",
                        mapper.writeValueAsString(tx), ex.getMessage(), ex);
            }
        } while (++retry <= retryCount);

        throw new IOException("Problem during sending and waiting transaction", origEx);
    }

    @Override
    public String send(Transaction tx) throws IOException {
        return communicate(true, new BaseCommunication<String>() {
            @Override
            public String call() throws IOException {
                return node.send(tx);
            }

            @Override
            public void onRetry(@Nullable String response, int retryNum) {
                LOGGER.trace("RETRY to send transaction: retryAttempt={} nodeUrl={} id={}",  retryNum, nodeUrl, tx.getId().toString());
            }
        });
    }

    @Override
    public String sendSafe(Transaction tx) {
        String txId = null;
        try {
            txId = send(tx);
        } catch (Throwable ex) {
            String failedTxId = tx != null ? tx.getId().toString() : "<empty>";
            LOGGER.error("Failure in sendSafe: tx_id={}", failedTxId , ex);
        }
        return txId;
    }

    @Override
    public Iterable<Transaction> getAllTransactions(String address, int pageSize) throws IOException {
        List<Transaction> initialTxs = getTransactions(address, pageSize, null);
        TransactionsIterator it = new TransactionsIterator(address, initialTxs, pageSize, this);
        return () -> it;
    }

    @Override
    public List<Transaction> getTransactions(String address, int pageSize, String afterTx) throws IOException {
        Objects.requireNonNull(address, "Address must not be null");
        if (pageSize <= 0) {
            throw new IllegalArgumentException("Page size must be greater than 0");
        }

        String url = nodeUrl + "/transactions/address/" + address + "/limit/" + pageSize;

        if (afterTx != null) {
            url += "?after=" + afterTx;
        }

        HttpGet req = new HttpGet(url);

        return communicate(true, new BaseCommunication<List<Transaction>>() {
            @Override
            public List<Transaction> call() throws IOException {
                try (CloseableHttpResponse resp = httpClient.execute(req)) {
                    if (resp.getStatusLine() != null
                            && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                            && resp.getEntity() != null) {
                        return mapper.mapTransactions(resp.getEntity().getContent());
                    } else {
                        HttpClientUtils.logBadResponse(resp, LOGGER);
                    }
                }
                @SuppressWarnings("unchecked")
                List<Transaction> empty = Collections.EMPTY_LIST;
                return empty;
            }

            @Override
            public boolean isSuccess(@Nonnull List<Transaction> response) {
                return Collections.EMPTY_LIST != response;
            }

            @Override
            public void onRetry(@Nullable List<Transaction> response, int retryNum) {
                LOGGER.trace("RETRY to obtain transactions: retryAttempt={} nodeUrl={} address={}",
                        retryNum, nodeUrl, address);
            }
        });
    }

    public Optional<List<DataEntry>> getAccountData(String accountAddress) throws IOException {
        String dataUrl = nodeUrl + "/addresses/data/" + accountAddress;

        HttpGet req = new HttpGet(dataUrl);

        return communicate(true, new BaseCommunication<Optional<List<DataEntry>>>() {
            @Override
            public Optional<List<DataEntry>> call() throws IOException {
                try (CloseableHttpResponse resp = httpClient.execute(req)) {
                    if (resp.getStatusLine() != null
                            && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                            && resp.getEntity() != null) {
                        return Optional.of(mapper.mapDataEntries(resp.getEntity().getContent()));
                    } else {
                        HttpClientUtils.logBadResponse(resp, LOGGER);
                    }
                }
                return Optional.empty();
            }

            @Override
            public void onRetry(@Nullable Optional<List<DataEntry>> response, int retryNum) {
                LOGGER.trace("RETRY to obtain data on account: retryAttempt={} nodeUrl={} address={}",
                        retryNum, nodeUrl, accountAddress);
            }
        });

    }

    @Override
    public Optional<DataEntry> getAccountDataByKey(String accountAddress, String key) throws IOException {
        String dataUrl = nodeUrl + "/addresses/data/" + accountAddress + "/" + key;

        HttpGet req = new HttpGet(dataUrl);

        return communicate(true, new BaseCommunication<Optional<DataEntry>>() {
            @Override
            public Optional<DataEntry> call() throws IOException {
                try (CloseableHttpResponse resp = httpClient.execute(req)) {
                    if (resp.getStatusLine() != null
                            && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                            && resp.getEntity() != null) {
                        return Optional.of(mapper.mapAny(resp.getEntity().getContent(),DataEntry.class));
                    } else {
                        HttpClientUtils.logBadResponse(resp, LOGGER);
                    }
                }
                return Optional.empty();
            }

            @Override
            public void onRetry(@Nullable Optional<DataEntry> response, int retryNum) {
                LOGGER.trace("RETRY to obtain data on account: retryAttempt={} nodeUrl={} address={}",
                        retryNum, nodeUrl, accountAddress);
            }
        });
    }

    @Override
    public List<DataEntry> getAccountDataByKeysList(String accountAddress, String... keys) throws IOException {
        StringBuilder keysBuilder = new StringBuilder(100);
        for (String key: keys) {
            if (keysBuilder.length() != 0) {
                keysBuilder.append("&");
            }
            keysBuilder.append("key=").append(key);
        }

        String url = nodeUrl + "/addresses/data/" + accountAddress + "?" + keysBuilder.toString();
        HttpGet req = new HttpGet(url);

        return communicate(true, new CustomResponseMappingCommunication<List<DataEntry>>(req, mapper::mapDataEntries) {
            @Override
            public void onRetry(@Nullable List<DataEntry> response, int retryNum) {
                LOGGER.trace("RETRY to obtain data on account by keys: retryAttempt={} nodeUrl={} address={} keys={}",
                        retryNum, nodeUrl, accountAddress, Arrays.toString(keys));
            }
        });
    }

    @Override
    public Map<String, Optional<DataEntry>> getAccountDataByKeysMap(String accountAddress, String... keys) throws IOException {
        List<DataEntry> dataEntries = getAccountDataByKeysList(accountAddress, keys);
        HashMap<String, Optional<DataEntry>> resultMap = new HashMap<>(keys.length * 2);

        for (String key: keys) {
            resultMap.put(key, dataEntries.stream().filter(d -> key.compareTo(d.getKey()) == 0).findFirst());
        }

        return resultMap;
    }

    @Override
    public List<DataEntry> getAccountDataByKeyRegExp(String accountAddress, String keyRegExp) throws IOException {
        String url = nodeUrl + "/addresses/data/" + accountAddress + "?matches=" + keyRegExp;
        HttpGet req = new HttpGet(url);

        return communicate(true, new CustomResponseMappingCommunication<List<DataEntry>>(req, mapper::mapDataEntries) {
            @Override
            public void onRetry(@Nullable List<DataEntry> response, int retryNum) {
                LOGGER.trace("RETRY to obtain data on account by keys: retryAttempt={} nodeUrl={} address={} keyRegExp={}",
                        retryNum, nodeUrl, accountAddress, keyRegExp);
            }
        });
    }

    @Override
    public Optional<DataEntry> getAccountDataByKeySilently(String accountAddress, String key) {
        try {
            return getAccountDataByKey(accountAddress, key);
        } catch (IOException ex) {
            LOGGER.warn("Muted exception because of silent mode", ex);
        }
        return Optional.empty();
    }

    @Override
    public CloseableHttpResponse sendJson(String json) throws IOException {
        return postJson(json, nodeUrl +"/transactions/broadcast");
    }

    public CloseableHttpResponse postJson(String json, String url) throws IOException {
        final HttpPost request = new HttpPost(url);
        request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));

        return communicate(true, new BaseCommunication<CloseableHttpResponse>() {
            @Override
            public CloseableHttpResponse call() throws IOException {
                return httpClient.execute(request);
            }

            @Override
            public boolean isSuccess(@Nonnull CloseableHttpResponse response) {
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    return true;
                } else {
                    HttpClientUtils.logBadResponse(response, LOGGER);
                    return false;
                }
            }

            @Override
            public void onRetry(@Nullable CloseableHttpResponse response, int retryNum) {
                LOGGER.trace("RETRY to send custom json data: retryAttempt={} nodeUrl={}", retryNum, nodeUrl);
                try {
                    if (response != null) {
                        response.close();
                    }
                } catch (IOException ex) {
                    LOGGER.warn("Unable to close CloseableHttpResponse object during RETRY operation - IGNORED", ex);
                }
            }
        });
    }

    @Override
    public BigDecimal waitMoneyOnBalance(String addressToCheck, BigDecimal initialAmount, BigDecimal amountToWait, BiPredicate<BigDecimal, BigDecimal> predicate, long timeToWait)
            throws IOException, InterruptedException {
        long remainingTime = timeToWait;
        long pause = 1000;
        BigDecimal expectedBalance = initialAmount.add(amountToWait);
        BigDecimal actualBalance;

        LOGGER.debug("Waiting balance: address={} initialAmount={} amountToWait={} time_to_wait={}",
                addressToCheck, initialAmount, amountToWait, formatInterval(remainingTime));
        do {
            long start = System.currentTimeMillis();
            if (remainingTime != timeToWait) {
                LOGGER.trace("Waiting balance CONTINUED: address={} initialAmount={} amountToWait={} time_to_wait={}",
                        addressToCheck, initialAmount, amountToWait, formatInterval(remainingTime));
                Thread.sleep(pause);
            }
            BalanceInfo balance = getBalanceInfo(addressToCheck);
            actualBalance = toServerMoney(balance.getAvailable());

            remainingTime -= (System.currentTimeMillis() - start);
        } while (!predicate.test(actualBalance, expectedBalance) && remainingTime > 0);

        if (remainingTime < 0) {
            LOGGER.error("Waited time is greater than specified timeout - waitMoneyOnBalance was interrupted: address={} initialAmount={} amountToWait={} actualAmount={} timeToWait={}",
                    addressToCheck, initialAmount, amountToWait, actualBalance, timeToWait);
            throw new BlkChTimeoutException("Waited time is greater than specified timeout " + timeToWait + " seconds");
        }

        return actualBalance;
    }

    @Override
    public Transaction waitTransaction(String txId) throws IOException, InterruptedException {
        return waitTransaction(txId, defaultWaitTimeout);
    }

    @Override
    public Transaction waitTransaction(String txId, long timeToWait) throws IOException, InterruptedException {
        return waitTransactions(Collections.singleton(txId), timeToWait);
    }

    @Override
    public Transaction waitTransactions(Set<String> txIds) throws IOException, InterruptedException {
        return waitTransactions(txIds, defaultWaitTimeout);
    }

    @Override
    public Transaction waitTransactions(Set<String> txIds, long timeToWait) throws IOException, InterruptedException {
        long remainingTime = timeToWait;
        long pause = 1000;
        Transaction txDetails = null;

        LOGGER.debug("Waiting transactions in block chain: txIds={} time_to_wait={}",
                txIds.toArray(), formatInterval(remainingTime));

        do {
            long start = System.currentTimeMillis();
            if (remainingTime != timeToWait) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.trace("Waiting transactions in block chain: txIds={} time_to_wait={}",
                            txIds.toArray(), formatInterval(remainingTime));
                }
                Thread.sleep(pause);
            }

            Iterator<String> txIter = txIds.iterator();
            while (txDetails == null && txIter.hasNext()) {
                try {
                    txDetails = getTransaction(txIter.next());
                } catch (Throwable ex) {
                    LOGGER.trace("Waiting transactions - unable to read tx by id: {}", ex.getMessage());
                }
            }

            remainingTime -= (System.currentTimeMillis() - start);
        } while (txDetails == null && remainingTime > 0);

        if (txDetails != null) {
            return txDetails;
        } else {
            LOGGER.error("Waited time is greater than specified timeout - waitTransaction was interrupted: txId={} timeToWait={}",
                    txIds.toArray(), timeToWait);
            throw new BlkChTimeoutException("Waited time is greater than specified timeout " + timeToWait + " seconds");
        }
    }

    @Override
    public <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate) throws InterruptedException, IOException {
        return whileInState(stateSupplier, loopContinuePredicate, o -> {});
    }

    @Override
    public <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate, Consumer<Optional<T>> timeoutCallback) throws InterruptedException, IOException {
        return whileInState(stateSupplier, loopContinuePredicate, defaultWhileInStateConfirms, defaultWaitTimeout, timeoutCallback);
    }

    @Override
    public <T> T whileInState(StateSupplier<Optional<T>> stateSupplier, Predicate<T> loopContinuePredicate, int confirmations, long timeout, Consumer<Optional<T>> timeoutCallback) throws InterruptedException, IOException {
        Optional<T> state = Optional.empty();
        long start = System.currentTimeMillis();
        int currConfirmations = 0;

        while (System.currentTimeMillis() - start <= timeout) {
            try {
                state = stateSupplier.get();
                if (state.isPresent()) {
                    if (!loopContinuePredicate.test(state.get())) {
                        if (++currConfirmations > confirmations) {
                            return state.get();
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.trace("whileInState: error during state read", ex);
            }

            long pause = currConfirmations == 0 ? 1000 : 100;
            Thread.sleep(pause);
        }

        timeoutCallback.accept(state);
        LOGGER.debug("Waiting state was stopped by timeout");
        throw new BlkChTimeoutException("Waiting state was stopped by timeout");
    }

    @Override
    public void sleepBlocks(int blocks) throws IOException, InterruptedException {
        waitHeight(getHeight() + blocks);
    }

    @Override
    public void sleepBlocks(int blocks, long maxWaitingTime) throws IOException, InterruptedException {
        waitHeight(getHeight() + blocks, maxWaitingTime);
    }

    @Override
    public void waitHeight(int heightToWait) throws IOException, InterruptedException {
        waitHeight(heightToWait, 0);
    }

    @Override
    public void waitHeight(int heightToWait, long maxWaitingTime) throws IOException, InterruptedException {
        int currentHeight = getHeight();
        int heightDiff = heightToWait - currentHeight;

        LOGGER.debug("Waiting height: heightToWait={} currentHeight={}",
                heightToWait, currentHeight);

        long estimatedScanTime = heightDiff * avgBlockDelay;
        maxWaitingTime = maxWaitingTime > 0 ? maxWaitingTime : estimatedScanTime * 5;

        long remainingTime = maxWaitingTime;
        long pause = 9 * avgBlockDelay / 10; // by default pause = 90% from average delay

        if (estimatedScanTime <= 0) {
            return;
        }

        long start = System.currentTimeMillis();
        while (heightToWait > (currentHeight = getHeight()) && remainingTime >= 0) {

            int currDiff = heightToWait - currentHeight;
            if (currDiff == 1) {
                // it means that remaining to wait last 1 block
                pause = avgBlockDelay / 10; // pause = 10% from average delay
            } else if (currDiff == 2) {
                pause = avgBlockDelay / 4; // pause = 25% from average delay
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.trace("Waiting height: heightToWait={} currentHeight={} pause={} remainingTime={}",
                        heightToWait, currentHeight, pause, formatInterval(remainingTime));
            }

            Thread.sleep(pause);

            long end = System.currentTimeMillis();
            remainingTime -= end - start;
            start = end;
        }

        if (remainingTime < 0) {
            throw new BlkChTimeoutException(String.format(
                    "Waiting time for specified height is greater than maxWaitingTime: heightToWait=%1$d currentHeight=%2$d maxWaitingTime=%3$s",
                    heightToWait, currentHeight, formatInterval(maxWaitingTime)));
        }
    }

    @Override
    public void logStatus() {
        PoolStats stats = connMng.getTotalStats();
        LOGGER.info("Node[{}]: http client connection pool status: {}", nodeUrl, stats.toString());
    }

    @Override
    public String getNodeInfo() {
        return nodeUrl;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public String getNodeUrl() {
        return nodeUrl;
    }


    protected <R> R communicate(boolean withRetry, Communicable<R> work) throws IOException {
        int retry = 0;
        IOException ioe = null;
        RuntimeException e = null;
        R lastResp;
        do {
            lastResp = null;
            try {
                if (retry != 0) {
                    Thread.sleep(1500);
                }
                lastResp = work.call();
                if (lastResp != null && work.isSuccess(lastResp)) {
                    return lastResp;
                } else {
                    retry(withRetry, work, lastResp, retry);
                }
            } catch (IOException ex) {
                retry(withRetry, work, lastResp, retry);
                ioe = ex;
                e = null;
            } catch (InterruptedException ex) {
                // todo need to stop
                retry(withRetry, work, lastResp, retry);
                ioe = new IOException("Retry attempt was stopped - terminate signal received", ex);
                e = null;
            }
            catch (Exception ex) {
                retry(withRetry, work, lastResp, retry);
                e = new RuntimeException("UNEXPECTED Exception during Waves API usage", ex);
            }
        } while (++retry < retriesCount && withRetry);

        if (lastResp != null) {
            return lastResp;
        } else if (e != null) {
            throw e;
        } else if (ioe != null) {
            throw ioe;
        } else {
            throw new RuntimeException("Broken logic in retry: lastResp and e and ioe are null");
        }
    }

    private <T> void retry(boolean withRetry, Communicable<T> work, @Nullable T response, int retry) {
        if (withRetry) {
            work.onRetry(response, retry);
        }
    }

    interface Communicable<T> {

        T call() throws IOException;

        boolean isSuccess(@Nonnull T response);

        /**
         * can be used to add logging or to clean up resources
         */
        void onRetry(@Nullable T response, int retryNum);
    }

    protected abstract class BaseCommunication<T> implements Communicable<T> {

        @Override
        public boolean isSuccess(@Nonnull T response) {
            return true;
        }
    }

    protected abstract class CustomResponseMappingCommunication<T> extends BaseCommunication<T> {
        private HttpGet req;
        private CustomMapper<InputStream, T> customMapper;

        protected CustomResponseMappingCommunication(HttpGet req, CustomMapper<InputStream, T> customMapper) {
            this.req = req;
            this.customMapper = customMapper;
        }

        @Override
        public T call() throws IOException {
            try (CloseableHttpResponse resp = httpClient.execute(req)) {
                if (resp.getStatusLine() != null
                        && resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                        && resp.getEntity() != null) {
                    return customMapper.map(resp.getEntity().getContent());
                } else {
                    HttpClientUtils.logBadResponse(resp, LOGGER);
                    throw new IOException("Bad response");
                }
            }
        }
    }

    interface CustomMapper<T, R> {
        R map(T t) throws IOException;
    }

    static class TransactionsIterator implements Iterator<Transaction> {

        static TransactionsIterator EMPTY = new TransactionsIterator() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Transaction next() {
                return null;
            }
        };

        private int size;
        private int pageSize;
        private boolean hasNext;
        private String address;
        private Iterator<Transaction> it;
        private NodeDecorator nodeDecorator;

        private TransactionsIterator() {
        }

        TransactionsIterator(String address, List<Transaction> initialTxs, int pageSize, NodeDecorator nodeDecorator) {
            this.address = address;
            this.it = initialTxs.iterator();
            this.size = initialTxs.size();
            this.pageSize = pageSize;
            this.nodeDecorator = nodeDecorator;
            hasNext = it.hasNext();
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public Transaction next() {
            Transaction next;
            if (hasNext) {
                next = it.next();
                hasNext = it.hasNext();
                if (!hasNext && size >= pageSize) {
                    try {
                        LOGGER.trace("Dynamic load for remaining transactions: address={} page_size={} after_tx={}",
                                address, pageSize, next.getId());
                        List<Transaction> txs = nodeDecorator.getTransactions(address, pageSize, next.getId().getBase58String());
                        LOGGER.trace("Dynamic load for remaining transactions: loaded={}", txs.size());
                        it = txs.iterator();
                        size = txs.size();
                        hasNext = it.hasNext();
                    } catch (IOException ex) {
                        throw new IllegalStateException(ex);
                    }
                }
                return next;
            } else {
                throw new NoSuchElementException("No more results");
            }
        }
    }

    private class WaitTransactionExecutable extends AlwaysSuccessExecutable<String> {

        private String txId;
        private long timeout;

        private WaitTransactionExecutable(String txId, long timeout) {
            this.txId = txId;
            this.timeout = timeout;
        }

        @Override
        public String call() throws IOException, InterruptedException {
            waitTransaction(txId, timeout);
            return txId;
        }

        @Override
        public String info() {
            return txId != null ? txId : "UNKNOWN";
        }
    }
}
