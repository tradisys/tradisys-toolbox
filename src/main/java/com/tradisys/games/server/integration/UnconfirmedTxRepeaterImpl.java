package com.tradisys.games.server.integration;

import com.wavesplatform.wavesj.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.tradisys.games.server.utils.FormatUtils.formatInterval;

public class UnconfirmedTxRepeaterImpl implements UnconfirmedTxRepeater {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnconfirmedTxRepeaterImpl.class);

    private NodeDecorator node;
    private ThreadPoolExecutor executor;

    public UnconfirmedTxRepeaterImpl(NodeDecorator node) {
        this.node = node;
        executor = new ThreadPoolExecutor(10, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS, new SynchronousQueue<>());
    }

    @Override
    public void push(String txId, Transaction origTx) {
        executor.execute(new RepeatTask(txId, origTx));
    }

    @Override
    public void confirm(String txId, Transaction origTx) {
        new RepeatTask(txId, origTx).run();
    }

    class RepeatTask implements Runnable {
        static final long TX_TimeToWait = 60 * 1000;
        static final int H_BlocksToWait = 2;
        static final long H_ProcTime = 20 * 60 * 1000;
        static final long H_CHECK_INTERVAL = 15 * 1000;

        String txId;
        Transaction origTx;

        RepeatTask(String txId, Transaction origTx) {
            this.txId = txId;
            this.origTx = origTx;
        }

        @Override
        public void run() {
            long hProcTime = H_ProcTime;
            int repeatThreshold = 5;

            int repeatNum = 0;
            boolean txConfirmed = false;
            while (repeatNum < repeatThreshold && !txConfirmed) {
                try {
                    if (repeatNum > 0) {
                        // resend transaction
                        LOGGER.warn("Repeater RESEND transaction: tx_id={}", txId);
                        txId = node.send(origTx);
                    }

                    int startHeight = node.getHeight();
                    int currentHeight = startHeight;
                    int prevHeight = currentHeight;
                    int heightToWait = startHeight + H_BlocksToWait;

                    node.waitTransaction(txId, TX_TimeToWait);

                    LOGGER.debug("Repeater START: tx_id={} startHeight={} currentHeight={} heightToWait={} remainingProcTime={}",
                            txId, startHeight, currentHeight, heightToWait, formatInterval(hProcTime));
                    do {
                        long hStartTime = System.currentTimeMillis();
                        Thread.sleep(H_CHECK_INTERVAL);
                        currentHeight = node.getHeight();
                        if (currentHeight > prevHeight) {
                            // test that tx in block chain
                            prevHeight = currentHeight;
                            LOGGER.debug("Repeater HEIGHT CHANGED: tx_id={} startHeight={} currentHeight={} heightToWait={} remainingProcTime={}",
                                    txId, startHeight, currentHeight, heightToWait, formatInterval(hProcTime));
                            node.waitTransaction(txId, TX_TimeToWait);
                        }
                        hProcTime -= System.currentTimeMillis() - hStartTime;
                    } while (currentHeight < heightToWait && hProcTime >= 0);

                    if (hProcTime >= 0) {
                        txConfirmed = true;
                        LOGGER.debug("Repeater FINISHED: tx_id={} startHeight={} currentHeight={} heightToWait={}",
                                txId, startHeight, currentHeight, heightToWait);
                    } else {
                        LOGGER.warn("Transaction disappeared from blockchain: tx_id={}", txId);
                    }
                } catch (Throwable ex) {
                    LOGGER.error("Repeater error: tx_id={}", txId, ex);
                } finally {
                    ++repeatNum;
                }
            }

            if (hProcTime < 0 || repeatNum >= repeatThreshold) {
                throw new RuntimeException("Transaction disappeared from blockchain: tx_id=" + txId);
            }
        }
    }
}
