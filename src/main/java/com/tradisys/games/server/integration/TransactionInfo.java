package com.tradisys.games.server.integration;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @deprecated use com.wavesplatform.wavesj.Transaction
 */
@Deprecated
public class TransactionInfo implements Serializable {

    private static final long serialVersionUID = 4019557793003808102L;

    public static final byte ISSUE         = 3;
    public static final byte TRANSFER      = 4;
    public static final byte REISSUE       = 5;
    public static final byte BURN          = 6;
    public static final byte LEASE         = 8;
    public static final byte LEASE_CANCEL  = 9;
    public static final byte ALIAS         = 10;
    public static final byte MASS_TRANSFER = 11;
    public static final byte DATA          = 12;
    public static final byte SET_SCRIPT    = 13;
    public static final byte SPONSOR       = 14;

    private String id;
    private byte type;
    private String sender;
    private String senderPublicKey;
    private long fee;
    private long timestamp;
    private String signature;
    private String[] proofs;
    private byte version;
    private String recipient;
    private long amount;
    private int height;
    private String assetId;
    private List<DataEntry> data;
    private String attachment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderPublicKey() {
        return senderPublicKey;
    }

    public void setSenderPublicKey(String senderPublicKey) {
        this.senderPublicKey = senderPublicKey;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String[] getProofs() {
        return proofs;
    }

    public void setProofs(String[] proofs) {
        this.proofs = proofs;
    }

    public byte getVersion() {
        return version;
    }

    public void setVersion(byte version) {
        this.version = version;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public List<DataEntry> getData() {
        return Collections.unmodifiableList(data);
    }

    public DataEntry getDataValue(String key) {
        return data.stream().filter(t -> t.key.equals(key)).findFirst().orElse(null);
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "TransactionInfo{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", sender='" + sender + '\'' +
                ", senderPublicKey='" + senderPublicKey + '\'' +
                ", fee=" + fee +
                ", timestamp=" + timestamp +
                ", version=" + version +
                ", recipient='" + recipient + '\'' +
                ", amount=" + amount +
                ", height=" + height +
                '}';
    }

    public static class DataEntry implements Serializable {

        private static final long serialVersionUID = 2625461450118546483L;
        private static final DataEntry EMPTY = new DataEntry();

        private String key;
        private String type;
        private String value;

        public DataEntry() {
        }

        public DataEntry(String key, String type, String value) {
            this.key = key;
            this.type = type;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }
    }
}
