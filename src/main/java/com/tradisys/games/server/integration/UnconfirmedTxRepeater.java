package com.tradisys.games.server.integration;

import com.wavesplatform.wavesj.Transaction;

public interface UnconfirmedTxRepeater {
    void push(String id, Transaction tx);

    void confirm(String id, Transaction tx);
}