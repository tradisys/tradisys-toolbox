package com.tradisys.games.server;

import com.tradisys.games.server.integration.NodeDecorator;
import com.tradisys.games.server.integration.ResponseMapper;
import com.tradisys.games.server.integration.TransactionInfo.DataEntry;
import com.tradisys.games.server.integration.WavesResponseMapper;
import com.wavesplatform.wavesj.Transaction;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.List;

public class WavesResponseMapperTest {

    private ResponseMapper responseMapper = new WavesResponseMapper(NodeDecorator.TESTNET);

    private static final String TRANSACTIONS_JSON_EMPTY = "[\n" +
            "  []\n" +
            "]";

    private static final String TRANSACTIONS_JSON = "[[ {\n" +
            "  \"type\" : 4,\n" +
            "  \"id\" : \"GTw3LiusMhp88s7HwdaTSvNbBJ2oR659pKXkF2p26BQc\",\n" +
            "  \"sender\" : \"3MwkAM78tUqHprSMfGT2JzdhYdwFNneEod8\",\n" +
            "  \"senderPublicKey\" : \"2jCcKA7sfqmjgAYnXwDpXb2YYhnQRSzvit2U7QjBQuoe\",\n" +
            "  \"fee\" : 500000,\n" +
            "  \"timestamp\" : 1547628768153,\n" +
            "  \"proofs\" : [ \"5zd8YHq2TgTyZBhS6UsCTT2V3tew5Uze2Q4rXETaQCxs1qX8JLAGHs8X6yWwxj3i5ywXRjpW3gTKehMzBhH6ZLjD\" ],\n" +
            "  \"version\" : 2,\n" +
            "  \"recipient\" : \"3MsT1dUWZ8fevrxs9PwAFLLMv6bbBQPyoTy\",\n" +
            "  \"assetId\" : null,\n" +
            "  \"feeAssetId\" : null,\n" +
            "  \"feeAsset\" : null,\n" +
            "  \"amount\" : 30000000,\n" +
            "  \"attachment\" : \"\",\n" +
            "  \"height\" : 452343\n" +
            "}, {\n" +
            "  \"type\" : 12,\n" +
            "  \"id\" : \"P8Ad7xL7ifrQGJH4Rac1BbvHedDWH8TeYUqCC7Qb8C6\",\n" +
            "  \"sender\" : \"3MwkAM78tUqHprSMfGT2JzdhYdwFNneEod8\",\n" +
            "  \"senderPublicKey\" : \"2jCcKA7sfqmjgAYnXwDpXb2YYhnQRSzvit2U7QjBQuoe\",\n" +
            "  \"fee\" : 500000,\n" +
            "  \"timestamp\" : 1547628759707,\n" +
            "  \"proofs\" : [ \"45bUtR9GAuTcUh4CT9ZWKL6xgstNR2dtURRUKwkAAdVbhFnJDbt5DmW8uN5GwfTpf3pgmpZixMKT2EoxpeXs9xBa\" ],\n" +
            "  \"version\" : 1,\n" +
            "  \"data\" : [ {\n" +
            "    \"key\" : \"serversChoice\",\n" +
            "    \"type\" : \"integer\",\n" +
            "    \"value\" : 3\n" +
            "  }, {\n" +
            "    \"key\" : \"serversSalt\",\n" +
            "    \"type\" : \"string\",\n" +
            "    \"value\" : \"536dcd29-13e7-4738-97cf-7f03372d70ef\"\n" +
            "  } ],\n" +
            "  \"height\" : 452343\n" +
            "}, {\n" +
            "  \"type\" : 12,\n" +
            "  \"id\" : \"Bp8ogDVpBJ2SkZLPVMP4JS61WzWvWvGUXPDtcZy6JGem\",\n" +
            "  \"sender\" : \"3MwkAM78tUqHprSMfGT2JzdhYdwFNneEod8\",\n" +
            "  \"senderPublicKey\" : \"2jCcKA7sfqmjgAYnXwDpXb2YYhnQRSzvit2U7QjBQuoe\",\n" +
            "  \"fee\" : 500000,\n" +
            "  \"timestamp\" : 1547628751159,\n" +
            "  \"proofs\" : [ \"3o6t1ehf9R3jyb7qsY8pGshwMJdoGZ23kNZXMqMPWzDNmNCArgF3Ad3RhPPBHMjR1Dvr9QZRKLAjfG2hV7M29hYL\" ],\n" +
            "  \"version\" : 1,\n" +
            "  \"data\" : [ {\n" +
            "    \"key\" : \"playersChoice\",\n" +
            "    \"type\" : \"integer\",\n" +
            "    \"value\" : 0\n" +
            "  } ],\n" +
            "  \"height\" : 452343\n" +
            "} ]]";

    private static final String DATA_ENTRIES_JSON = "[\n" +
            "  {\"type\": \"integer\", \"value\": 100854, \"key\": \"price_index\"},\n" +
            "  {\"type\": \"integer\", \"value\": 100, \"key\": \"price\"},\n" +
            "  {\"type\": \"integer\", \"value\": 101, \"key\": \"price_2005795\"}\n" +
            "]";

    @Test
    public void testTransactionsMappingResponse() throws Exception {
        List<Transaction> txs = responseMapper.mapTransactions(new ByteArrayInputStream(TRANSACTIONS_JSON.getBytes()));
        Assert.assertNotNull("Transactions should not be null", txs);
        Assert.assertEquals("Transactions count should be 3", 3, txs.size());
    }

    @Test
    public void testTransactionsMappingEmptyResponse() throws Exception {
        List<Transaction> txs = responseMapper.mapTransactions(new ByteArrayInputStream(TRANSACTIONS_JSON_EMPTY.getBytes()));
        Assert.assertNotNull("Transactions should not be null", txs);
        Assert.assertEquals("Transactions count should be 3", 0, txs.size());
    }

    @Test
    public void testDataEntriesMappingResponse() throws Exception {
        List<DataEntry> dataEntries = responseMapper.mapDataEntries(new ByteArrayInputStream(DATA_ENTRIES_JSON.getBytes()));
        Assert.assertNotNull("Transactions should not be null", dataEntries);
        Assert.assertEquals("Transactions count should be 3", 3, dataEntries.size());
    }
}
