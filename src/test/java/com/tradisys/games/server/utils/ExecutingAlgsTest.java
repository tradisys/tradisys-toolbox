package com.tradisys.games.server.utils;

import com.tradisys.games.server.exception.BlkChTimeoutException;
import com.tradisys.games.server.utils.ExecutingAlgs;
import com.tradisys.games.server.utils.ExecutingAlgs.Executable;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExecutingAlgsTest {

    private static Logger LOGGER = LoggerFactory.getLogger(ExecutingAlgs.class);

    private static final long DEFAULT_TIMEOUT_DIFF = 5 * 1000;
    private static final long DEFAULT_TIMEOUT = 60 * 1000;
    private static final int TASKS = 3;

    @Test
    public void testPositiveScenario() throws Exception {
        List<BasicTestExecutable> tasks = new ArrayList<>();
        for (int i = 0; i < TASKS; i++) {
            BasicTestExecutable task = new BasicTestExecutable(i, (i + 1) * DEFAULT_TIMEOUT_DIFF);
            tasks.add(task);
        }

        Integer id = ExecutingAlgs.firstSuccessTask(tasks, DEFAULT_TIMEOUT);
        Assert.assertEquals("Test that first task finished", 0, id.intValue());

        // small pause to wait other processes
        Thread.sleep(2 * 1000);
        for (int i = 1; i < TASKS; i++) {
            BasicTestExecutable task = tasks.get(i);
            LOGGER.info(task.info());
            Assert.assertTrue("Test that task #" + i + " was interrupted", task.interrupted);
        }
    }

    @Test
    public void testAllFailuresExceptLast() throws Exception {
        int lastId = TASKS - 1;
        List<BasicTestExecutable> tasks = new ArrayList<>();
        for (int i = 0; i < lastId; i++) {
            BasicTestExecutable task = new FailTestExecutable(i, (i + 1) * DEFAULT_TIMEOUT_DIFF);
            tasks.add(task);
        }
        tasks.add(new BasicTestExecutable(lastId, TASKS * DEFAULT_TIMEOUT_DIFF));

        Integer id = ExecutingAlgs.firstSuccessTask(tasks, DEFAULT_TIMEOUT);
        Assert.assertEquals("Test that last task finish with success", lastId, id.intValue());
    }

    @Test
    public void testTimeout() throws Exception {
        long timeout = 10 * 1000;
        long diff = timeout + 5 * 1000;
        List<BasicTestExecutable> tasks = new ArrayList<>();
        for (int i = 0; i < TASKS; i++) {
            BasicTestExecutable task = new BasicTestExecutable(i, (i + 1) * diff);
            tasks.add(task);
        }

        boolean exception = false;
        try {
            ExecutingAlgs.firstSuccessTask(tasks, timeout);
        } catch (BlkChTimeoutException ex) {
            exception = true;
        }

        Assert.assertTrue("Timeout exception occurred", exception);

        // small pause to wait other processes
        Thread.sleep(2 * 1000);
        for (int i = 0; i < TASKS; i++) {
            BasicTestExecutable task = tasks.get(i);
            LOGGER.info(task.info());
            Assert.assertTrue("Test that task #" + i + " was interrupted", task.interrupted);
        }
    }

    @Test
    public void testWithUnSuccessResults() throws Exception {
        List<BasicTestExecutable> tasks = new ArrayList<>();
        int middle = TASKS / 2;
        int lastId = TASKS - 1;
        for (int i = 0; i < TASKS; i++) {
            long timeout = (i + 1) * DEFAULT_TIMEOUT_DIFF;
            BasicTestExecutable task;
            if (i < middle) {
                task = new FailTestExecutable(i, timeout);
            } else if (i < lastId) {
                task = new UnSuccessResultExecutable(i, timeout);
            } else {
                task = new BasicTestExecutable(i, timeout);
            }
            tasks.add(task);
        }

        Integer id = ExecutingAlgs.firstSuccessTask(tasks, DEFAULT_TIMEOUT);
        Assert.assertEquals("Test that finished task valid", lastId, id.intValue());
    }

    private static class BasicTestExecutable implements Executable<Integer> {

        int id;
        private int result = -1;
        private long timeout;
        private boolean interrupted;

        BasicTestExecutable(int id, long timeout) {
            this.id = id;
            this.timeout = timeout;
        }

        @Override
        public Integer call() throws IOException {
            long remaining = timeout;
            long pause = 1000;

            try {
                if (timeout > 0) {
                    do {
                        Thread.sleep(pause);
                        remaining -= pause;
                    } while (remaining > 0);
                }
            } catch (InterruptedException ex) {
                LOGGER.info("task # {} was interrupted", id);
                interrupted = true;
            }

            if (interrupted) {
                result = -1;
            } else {
                result = id;
            }

            return result;
        }

        @Override
        public boolean isSuccess(Integer resp) {
            return true;
        }

        @Override
        public String info() {
            return "id=" + id + " interrupted=" + interrupted + " timeout=" + timeout;
        }
    }

    private static class FailTestExecutable extends BasicTestExecutable {

        public FailTestExecutable(int id, long timeout) {
            super(id, timeout);
        }

        @Override
        public Integer call() throws IOException {
            super.call();
            throw new IOException("" + id);
        }
    }

    private static class UnSuccessResultExecutable extends  BasicTestExecutable {
        public UnSuccessResultExecutable(int id, long timeout) {
            super(id, timeout);
        }

        @Override
        public boolean isSuccess(Integer resp) {
            return false;
        }
    }
}
