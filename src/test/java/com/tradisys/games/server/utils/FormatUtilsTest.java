package com.tradisys.games.server.utils;

import com.tradisys.games.server.exception.BlkChTimeoutException;
import com.tradisys.games.server.utils.ExecutingAlgs.Executable;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class FormatUtilsTest {

    private static Logger LOGGER = LoggerFactory.getLogger(ExecutingAlgs.class);


    @Test
    public void testCorrectFormat() throws Exception {
        Assert.assertEquals(95l,FormatUtils.toBlkAmount(BigDecimal.valueOf(0.95),2));
        Assert.assertEquals(BigDecimal.valueOf(0.95),FormatUtils.toServerAmount(95,2).setScale(2, RoundingMode.DOWN));

    }


}
